import http from '@/config/fetch'
import {getStore} from '../config/mUtils'
import axios from 'axios'

/**
 * sign
*/
export const signup = param => {
    return http.fetchPost("/signup", param)
}

//激活账户
export const activeAccount = param => {
    return http.fetchGet("/active_account", param)
}

/**
 * login
*/
export const login = param => {
    return http.fetchPost("/signin", param)
}

// signout
export const signout = param => {
    return http.fetchPost("/signout", param)
}

/**
 * 获取用户信息
 */
export const getUser = param => {
    return http.fetchGet("/getUserById", param)
}

/**
 * 根据用户名获取用户信息
 */
export const getUserByName = param => {
    return http.fetchGet("/getUserByName", param)
}

//更新设置用户信息
export const updateUserInfo = param => {
    return http.fetchPost('/user/updateInfo', param)
}

//topic
//创建新话题或修改话题,==>根据是否传topic_id判断
export const createTopic = param => {
    return http.fetchPost("/topic/create", param)
}

//删除某话题
export const delTopic = param => {
    return http.fetchPost("/topic/delete", param)
}

//获取话题
export const getTopic = param => {
    return http.fetchGet("/topic", param)
}

//获取热文榜
export const getTopicTop = param => {
    return http.fetchGet("/topicTop", param)
}

//获取板块类型
export const topicType = param => {
    return http.fetchGet('/topicType', param)
}

//根据_id获取话题
export const getTopicDetail = param => {
    return http.fetchGet("/topicDetail", param)
}

//查看近期发布的文章
export const topicByUser = param => {
    return http.fetchGet("/topicByUser", param)
}

//收藏某话题
export const collectTopic = param => {
    return http.fetchPost("/topic/collect", param)
}

//取消收藏
export const removeCollect = param => {
    return http.fetchPost("/topic/removeCollect", param)
}

//获取用户收藏的话题
export const getCollections = param => {
    return http.fetchGet('/user/collections', param)
}

//获取用户参与的话题
export const getListReplies = param => {
    return http.fetchGet('/user/listReplies', param)
}

//回复话题
export const replyTopic = param => {
    return http.fetchPost("/reply", param)
}

//删除回复
export const delReply = param => {
    return http.fetchPost('/reply/delete', param)
}

//修改回复内容
export const replyUpdate = param => {
    return http.fetchPost('/reply/update', param)
}

//上传图片
export const imgUpload = param => {
    return http.fetchUpload('/upload', param)
}