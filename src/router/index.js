import Vue from 'vue'
import Router from 'vue-router'
import { getStore } from '../config/mUtils'
import { login } from '../service/api';

Vue.use(Router)

const Index = r => require.ensure([], () => r(require('@/pages/index/index')), 'index')
const Sign = r => require.ensure([], () => r(require('@/pages/sign/sign')), 'sign')
const Login = r => require.ensure([], () => r(require('@/pages/login/login')), 'login')
const User = r => require.ensure([], () => r(require('@/pages/user/user')), 'user')
const Setting = r => require.ensure([], () => r(require('@/pages/user/setting')), 'setting')
const TopicCreate = r => require.ensure([], () => r(require('@/pages/topic/create')), 'create')
const TopicDetail = r => require.ensure([], () => r(require('@/pages/topic/detail')), 'detail')
const UserCollections = r => require.ensure([], () => r(require('@/pages/user/userCollections')), 'userCollections')
const UserTopics = r => require.ensure([], () => r(require('@/pages/user/userTopics')), 'userTopics')
const UserReply = r => require.ensure([], () => r(require('@/pages/user/userReply')), 'userReply')
const TopicEdit = r => require.ensure([], () => r(require('@/pages/topic/topicEdit')), 'topicEdit')
const ActiveAccount = r => require.ensure([], () => r(require('@/pages/user/activeAccount')), 'activeAccount')

// const Index = resolve => require(['@/pages/index/index'], resolve)
// const Sign = r => require(['@/pages/sign/sign'], r)
// const Login = r => require(['@/pages/login/login'], r)
// const User = r => require(['@/pages/user/user'], r)
// const Setting = r => require(['@/pages/user/setting'], r)
// const TopicCreate = r => require(['@/pages/topic/create'], r)
// const TopicDetail = r => require(['@/pages/topic/detail'], r)
// const UserCollections = r => require(['@/pages/user/userCollections'], r)
// const UserTopics = r => require(['@/pages/user/userTopics'], r)
// const UserReply = r => require(['@/pages/user/userReply'], r)
// const TopicEdit = r => require(['@/pages/topic/topicEdit'], r)
// const ActiveAccount = r => require(['@/pages/user/activeAccount'], r)

const router = new Router({
  routes: [
    {
      path: '/',  // 默认进入路由
      redirect: '/home'   //重定向
    },
    {
      path: '/home',
      component: Index,
      name: 'home'
    },
    {
      path: '/sign',
      component: Sign,
      // name: 'sign'
    }, 
    {
      path: '/active/:name/:key',
      component: ActiveAccount
    },
    {
      path: '/login',
      component: Login,
      name: 'login'
    },
    {
      path: '/user/:name',
      component: User,
      // name: 'user'
    },
    {
      path: '/topic/create',
      component: TopicCreate,
      meta: { requiresAuth: true },
      // name: 'topicCreate'
    },
    {
      path: '/topic/:tid',
      component: TopicDetail
    },
    {
      path: '/topic/:tid/edit',
      component: TopicEdit,
      meta: { requiresAuth: true },
      // name: 'topidEdit'
    },
    {
      path: '/user/:name/collect',
      component: UserCollections,
      meta: { requiresAuth: true },
      // name: 'collect'
    },
    {
      path: '/user/:name/topics',
      component: UserTopics,
      meta: { requiresAuth: true },
    },
    {
      path: '/user/:name/reply',
      component: UserReply,
      meta: { requiresAuth: true },
    },
    {
      path: '/setting',
      component: Setting,
      meta: { requiresAuth: true },
      // name: 'setting'
    }
  ]
})

// 全局路由守卫
router.beforeEach((to, from, next) => {
  // console.log(to.matched, to.fullPath)
  let isLogin = getStore('user_id') ? true : false

  // 未登录状态；当路由到nextRoute指定页时，跳转至login
  if (to.matched.some(record => record.meta.requiresAuth)) {
    if(!isLogin) {
      next({
        path: '/login',
        query: { redirect: to.fullPath}
      })
    } else {
      next()
    }
  } else {
    next()
  }
  // 已登录状态；当路由到login时，跳转至home
  if (to.name === 'login' && isLogin) {
    if (isLogin) {
      router.push({ name: 'home' });
    }
  } else {
    next()
  }

});

export default router
